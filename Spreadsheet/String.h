//
// Created by shrexpecial on 12.05.19.
//

#ifndef SPREADSHEET_STRING_H
#define SPREADSHEET_STRING_H

#include <string>
#include "Cell.h"
/***
 * A class containing all the cells of type String
 */
class String: public Cell {
    std::string data; /*!The cell value formatted without quotes and character escapings */
    std::string outputData; /*!The value of the cell as plain text*/
public:
    String();
    explicit String(std::string input);
    void print() override;
    void set(std::string input) override;
    std::string get() override;
    void save(std::ofstream &file) override;
    //Default destructor as no dynamic memory allocation occurs
    ~String() override = default;
};


#endif //SPREADSHEET_STRING_H
