//
// Created by shrexpecial on 30.05.19.
//

#ifndef SPREADSHEET_SPREADSHEET_H
#define SPREADSHEET_SPREADSHEET_H

#include <fstream>
#include "Table.h"
/**
 * An enum describing all command types
 */
enum Commands{
    OPEN,
    SAVE,
    SAVEAS,
    ADDCOL,
    ADDROW,
    CREATE,
    CLOSE,
    EXIT,
    PRINT,
    EDIT,
    INVALID
};
/**
 * A class that represents a single spreadsheet tab
 */
 //TODO: Make the class start a new thread whenever the program opens a new spreadsheet
class Spreadsheet {
    bool running; /*!Shows whether the program is running */
    std::string file; /*!Represents the file, the user is currently working with */
    Table* currentTable; /*!Pointer to the currently opened table*/
    std::ofstream outputFile; /*! The file we wish to save our table to*/
    bool unsaved; /*!Represents whether any unsaved changes have been made to the file */

    /**
     * What is the type of the current command
     * @param command - command as plain text
     * @return - command as enum
     */
    static Commands getCommand(const std::string& command);
public:
    Spreadsheet(){
        this->running = true;
        this->unsaved = false;
        this->file = "";
    }
    /**
     * Initialize the program
     */
    void runSpreadsheet();
    /**
     * Opens a specific file by its path
     * @param filename - the path for the desired file
     */
    void openFile(std::string filename);
    /**
     * Saves all the changes to the current file
     */
    void saveFile();
    /**
     * Edits the current table without saving
     */
    void editFile();
    /**
     * Save the current table to a specific file
     * @param path - path for the desired output file
     */
    void saveAsFile(const std::string& path);
    /**
     * @return - is there currently an open file
     */
    bool isOpen();
    /**
     * Closes the currently opened file checking whether it's been saved
     */
    void closeFile();
    /**
     * Creates an empty table
     */
    void createTable();
};


#endif //SPREADSHEET_SPREADSHEET_H
