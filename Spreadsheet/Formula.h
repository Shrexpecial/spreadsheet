//
// Created by shrexpecial on 12.05.19.
//

#ifndef SPREADSHEET_FORMULA_H
#define SPREADSHEET_FORMULA_H

#include <string>
#include "Cell.h"
#include "Table.h"
/**
 * A class containing all the cells with formulas
 */
class Formula: public Cell {
    std::string data; /*! holds the formula of the cell */
    std::string outputData; /*! holds the plain text value of the cell */
    double value; /*! holds the numeric value of the cell*/
public:
    /**
     * Default constructor
     */
    Formula();
    /**
     * Constructor with parameters
     * @param input - the formula as plain text
     */
    explicit Formula(std::string input);
    void convert(double value) override;
    void print() override;
    void set(std::string input) override;
    void save(std::ofstream &file) override;
    std::string get() override;
    std::string getFormula() override;
    //Default destructor as no dynamic memory allocation occurs
    ~Formula() override = default;
};


#endif //SPREADSHEET_FORMULA_H
