//
// Created by shrexpecial on 12.05.19.
//

#include <iostream>
#include <iomanip>
#include "Integer.h"
//Constructor with parameters
Integer::Integer(int input) {
    this->data = input;
    this->type = INTEGER;
}
//Default constructor
Integer::Integer() {
    this->data = 0;
    this->type = INTEGER;
}
//Prints the value of the cell
void Integer::print() {
    std::cout << this->data;
}
//Sets the value of the cell
void Integer::set(std::string a) {
    this->data = std::stoi(a);
}
//Returns the value of the cell as a string
std::string Integer::get() {
    return std::to_string(this->data);
}
