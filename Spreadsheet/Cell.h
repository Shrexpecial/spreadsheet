//
// Created by shrexpecial on 12.05.19.
//

#ifndef SPREADSHEET_CELL_H
#define SPREADSHEET_CELL_H

#include <string>
/***
 * This is an abstract class, meant for all the cells to inherit.
 * The Type enum is meant to represent the type of each cell.
 */
enum Type{
    FORMULA,
    INTEGER,
    DOUBLE,
    STRING,
    BLANK,
    ERROR
};
class Cell {
protected:
    Type type; /*! Holds the cell type. */
public:

    /**
     * @return - type of the cell
     */
    Type getType();

    /**
     * prints the content of the cell
     */
    virtual void print() = 0;

    /**
     * @return - the cell value
     */
    virtual std::string get() = 0;

    /**
     * Sets the desired value to the cell value
     * @param input - desired value
     */
    virtual void set(std::string input);

    /**
     * Converts the formula expression to a numeric value
     * @param value
     */
    virtual void convert(double value);

    /**
     * Saves the content of a cell to a certain file
     * @param file - desired file
     */
    virtual void save(std::ofstream &file);

    /**
     * @return - the formula from a formula cell type
     */
    virtual std::string getFormula();

    /**
     * @return - the length of the cell value
     */
    virtual int getLen();

    /**
     *Making the destructor virtual so all the subclasses get deleted properly
     */
    virtual ~Cell();
};


#endif //SPREADSHEET_CELL_H
