#include <utility>

#include <utility>

//
// Created by shrexpecial on 30.05.19.
//

#include <string>
#include <iostream>
#include "Spreadsheet.h"
#include "HelperFunctions.h"

Commands Spreadsheet::getCommand(const std::string& command) {
    if(command == "open"){
        return OPEN;
    }
    else if(command == "save"){
        return SAVE;
    }
    else if(command == "saveas"){
        return SAVEAS;
    }
    else if(command == "close"){
        return CLOSE;
    }
    else if(command == "exit"){
        return EXIT;
    }
    else if(command =="print"){
        return PRINT;
    }
    else if(command =="edit"){
        return EDIT;
    }
    else if(command =="addcol"){
        return ADDCOL;
    }
    else if (command =="addrow") {
        return ADDROW;
    }
    else if (command == "create") {
        return CREATE;
    }
    else {
        return INVALID;
    }
}

void Spreadsheet::runSpreadsheet() {
    std::string input;
    while (this->running){
        getline(std::cin >> std::ws, input);
        std::vector<std::string> command = split(input, ' ');
        Commands currentCommand = INVALID;
        try {
            currentCommand = getCommand(command.at(0));
        }catch (std::out_of_range &outOfRange){
            currentCommand = INVALID;
        }
        switch (currentCommand){
            case OPEN:
                if(input.substr(input.length() - 4) == ".txt"){
                    if(!this->file.empty()){
                        delete(this->currentTable);
                    }
                    this->openFile(input.substr(5));
                } else {
                    std::cout << "Unknown file type. This program works with .txt files only." << std::endl;
                }
                break;
            case CLOSE:
                if(isOpen()){
                    this->closeFile();
                } else std::cout << "No file currently opened." << std::endl;
                break;
            case SAVE:
                if(isOpen()){
                    this->saveFile();
                } else std::cout << "No file currently opened." << std::endl;
                break;
            case SAVEAS:
                if(isOpen()){
                    this->saveAsFile(input.substr(7));
                } else std::cout << "No file currently opened." << std::endl;
                break;
            case EXIT:
                if(this->unsaved){
                    char query;
                    std::cout << this->file + " is currently opened. Are you sure you want to exit? "
                                              "You can choose to save the file now. [Y/n/s]" << std::endl;
                    std::cin >> query;
                    if(query =='n' || query == 'N') break;
                    else if (query == 's' || query == 'S') {
                        this->saveFile();
                    }
                }
                std::cout << "Exiting the program...";
                this->running = false;
                break;
            case INVALID:
                std::cout << "Not a valid command." << std::endl;
                break;
            case EDIT:
                editFile();
                break;
            case ADDCOL:
                this->currentTable->addColumn();
                this->unsaved = true;
                break;
            case ADDROW:
                this->currentTable->addRow();
                this->unsaved = true;
                break;
            case CREATE:
                this->createTable();
                break;
            case PRINT:
                if(isOpen()){
                    this->currentTable->print();
                } else std::cout << "No file currently opened." << std::endl;
                break;
            default:
                break;
        }
    }
}

void Spreadsheet::openFile(std::string filename) {
    this->file = std::move(filename);
    this->currentTable = new Table(this->file);
}

void Spreadsheet::saveFile() {
    outputFile.open(this->file, std::ios::trunc);
    this->currentTable->save(outputFile);
    outputFile.close();
    this->unsaved = false;
    std::cout << "File " + this->file + " successfully saved" << std::endl;
}

void Spreadsheet::saveAsFile(const std::string& path) {
        outputFile.open(path, std::ios::trunc);
        this->currentTable->save(outputFile);
        outputFile.close();
        this->unsaved = false;
        std::cout << "File " + path + " successfully saved" << std::endl;
}

void Spreadsheet::closeFile() {
    if(this->unsaved){
        std::cout << "There are unsaved changes. Are you sure you want to close "+this->file + "? [Y/n/s]" << std::endl;
        char query;
        std::cin >> query;
        if(query == 'N' || query == 'n') {
            std::cout << "Resuming work on the current file." << std::endl;
        } else if (query == 's' || query == 'S') {
            this->saveFile();
            this->unsaved = false;
            std::cout << "File "+  this->file + " successfully closed." << std::endl;
            this->file = "";
            this->unsaved = false;
            delete(this->currentTable);
        } else {
            std::cout << "File "+  this->file + " successfully closed." << std::endl;
            this->file = "";
            this->unsaved = false;
            delete(this->currentTable);
        }
    } else {
        std::cout << "File "+  this->file + " successfully closed." << std::endl;
        this->unsaved = false;
        delete(this->currentTable);
        this->file = "";
    }
}

bool Spreadsheet::isOpen() {
    return !this->file.empty();
}

void Spreadsheet::editFile() {
    int col, row;
    std::string value;
    std::cout << "Please enter the row and column of the cell you want to edit."<<std::endl
    << "The input should be in the following format [row] [column]:" << std::endl;
    std::cin >> row >> col;
    std::cout << "Enter the desired value for R" << row << "C" << col << ":" <<std::endl;
    getline(std::cin >> std::ws , value);
    this->currentTable->set(row, col, value);
    this->unsaved = true;
}

void Spreadsheet::createTable() {
    std::cout << "Enter the desired height and length for your table. [height] [length] " << std::endl;
    int h, l;
    std::cin >> h >> l;
    if(isOpen()){
        this->closeFile();
    }
    this->file = "NewFile.txt";
    this->unsaved = true;
    this->currentTable = new Table(h, l);
}

