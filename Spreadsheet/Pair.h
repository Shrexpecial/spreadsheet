//
// Created by shrexpecial on 03.06.19.
//

#ifndef SPREADSHEET_PAIR_H
#define SPREADSHEET_PAIR_H
/**
 * A structure defining the coordinates of a cell
 */
struct Pair{
    int row;
    int col;
};
#endif //SPREADSHEET_PAIR_H
