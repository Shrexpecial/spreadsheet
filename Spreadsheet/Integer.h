//
// Created by shrexpecial on 12.05.19.
//

#ifndef SPREADSHEET_INTEGER_H
#define SPREADSHEET_INTEGER_H


#include "Cell.h"
/***
 * A class containing all the cells of type integer
 */
class Integer : public Cell{
    int data; /*! Holds the numeric value of the current cell */
public:
    /**
     * A default constructor
     */
    Integer();
    /**
     * A constructor with parameters
     * @param input - the desired cell value
     */
    explicit Integer(int input);
    void print();
    void set(std::string a);
    std::string get() override;
    //Default destructor as no dynamic memory allocation occurs
    ~Integer() override = default;
};


#endif //SPREADSHEET_INTEGER_H
