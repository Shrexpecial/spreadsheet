//
// Created by shrexpecial on 03.06.19.
//

#ifndef SPREADSHEET_EVALUATOR_H
#define SPREADSHEET_EVALUATOR_H


#include <string>
#include <cmath>
#include <iostream>
/**
 * A simple recursive descent parser.
 * Found on StackOverflow but highly tweaked to accommodate the needs of the project.
 */
class Evaluator {
    const char *expressionToParse;
    double value;
    /**
     * @return - current character in the expression
     */
    char peek()
    {
        return *expressionToParse;
    }
    /**
     * @return - current character and moves the pointer to the next character
     */
    char get()
    {
        return *expressionToParse++;
    }
    /**
     * @return - the value of the number at which we currently are
     */
    double number()
    {
        std::string res;
        res += get();
        while ((peek() >= '0' && peek() <= '9') || peek() == '.')
        {
            res += get();
        }
        return std::stod(res);
    }
    /**
     * @return - lowest priority operation
     */
    double factor()
    {
        if (peek() >= '0' && peek() <= '9')
            return number();
        else if (peek() == '(')
        {
            get(); // '('
            double result = expression();
            get(); // ')'
            return result;
        }
        else if (peek() == '-')
        {
            get();
            return -factor();
        }
        return 0; // error
    }
    /**
     * @return - value of highest priority operation
     */
    double term()
    {
        double result = factor();
        while (peek() == '*' || peek() == '/' || peek() == '^'){
            if (peek() == '*'){
                get();
                result *= factor();
            }
            else if (get() == '^') {
                result = pow(result, factor());
            }
            else result /= factor();
        }
        return result;
    }
    /**
     * @return value of the whole expression
     */
    double expression()
    {
        double result = term();
        while (peek() == '+' || peek() == '-')
            if (get() == '+')
                result += term();
            else
                result -= term();
        return result;
    }

public:
    /**
     * Constructor that removes spaces and letters from the string
     * @param input - string containing the formula for parsing
     */
    explicit Evaluator(std::string input){
        std::string trimmed; // Get Rid of Spaces
        for (int i = 0; i < input.length(); i++)
        {
            if (input[i] != ' ' &&
            !(input[i] >= 'a' && input[i] <= 'z') &&
            !(input[i] >= 'A' && input[i] <= 'Z'))
            {
                trimmed += input[i];
            }
        }
        this->expressionToParse = trimmed.c_str();
        this->value = expression();
    }
    double getValue(){ return this->value;}
};


#endif //SPREADSHEET_EVALUATOR_H
