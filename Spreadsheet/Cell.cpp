//
// Created by shrexpecial on 12.05.19.
//

#include <fstream>
#include <iostream>
#include "Cell.h"


void Cell::set(std::string input) {
}

Type Cell::getType() {
    return this->type;
}

void Cell::convert(double value) {
}

void Cell::save(std::ofstream &file) {
    file << this->get() + " ";
}

std::string Cell::getFormula() {
    return "0";
}

int Cell::getLen() {
    return this->get().length();
}

Cell::~Cell() = default;
