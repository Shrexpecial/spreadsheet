//
// Created by shrexpecial on 12.05.19.
//

#ifndef SPREADSHEET_TABLE_H
#define SPREADSHEET_TABLE_H


#include <vector>
#include "Row.h"
#include "Pair.h"
/***
 * A class that represent a whole table.
 */
class Table {
    std::vector<Row*> table; /*!A vector of rows that holds all the cells in the table. */
    int max_len;/*!The length of the longest row in the table. */
    int height; /*!The table's height. */
    std::vector<int> maxColLength; /*!The length of the longest word in the table for each column. */
    std::vector<Pair*> formulas;/*!A vector containing the coordinates of all formulas. */
public:
    /**
     * Default constructor.
     */
    Table();
    /**
     * A constructor that creates an empty height x length table.
     * @param height - desired height.
     * @param length - desired length.
     */
    Table(int height, int length);
    /**
     * A constructor that creates a table from a file.
     * @param filename - the path to the desired file.
     */
    explicit Table(const std::string& filename);
    /**
     * Prints the content of the table.
     */
    void print();
    /**
     * Returns the value of the cell at a specified position.
     * @param h - row of the cell.
     * @param l - column of the cell.
     * @return - value of the cell at [h, l].
     */
    std::string get(int h, int l);
    /**
     * Sets the value of a cell at a specified position.
     * @param height - row of the cell.
     * @param length - column of the cell.
     * @param value - desired value.
     */
    void set(int height, int length, const std::string& value);
    /**
     * Saves the current table to a specified file.
     * @param file - path to the desired file.
     */
    void save(std::ofstream &file);
    /**
     * Converts all formulas to their numeric value.
     */
    void convertFormulas();
    /**
     * Adds an empty row to the table.
     */
    void addRow();
    /**
     * Adds an empty column to the table.
     */
    void addColumn();
    /**
     * Destructor to free all manually allocated memory
     */
    ~Table();
};


#endif //SPREADSHEET_TABLE_H
