//
// Created by shrexpecial on 12.05.19.
//

#ifndef SPREADSHEET_DOUBLE_H
#define SPREADSHEET_DOUBLE_H

#include "Cell.h"


/***
 * A cell containing numbers of type double
 */
class Double: public Cell {
    double data; /**The numeric value of the cell*/
public:

    /**
     * Default constructor
     */
    Double();

    /***
     * Constructor with params
     * @param input - the desired cell value
     */
    explicit Double(double input);

    void print();

    void set(std::string a);

    std::string get();

    ~Double() override = default;
};


#endif //SPREADSHEET_DOUBLE_H
