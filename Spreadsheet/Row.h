//
// Created by shrexpecial on 12.05.19.
//

#ifndef SPREADSHEET_ROW_H
#define SPREADSHEET_ROW_H


#include <vector>
#include "Cell.h"
#include "Pair.h"

/**
 * A class that represents a single row in our table.
 */

class Row {
    std::vector<Cell*> row; /*!< A vector holding the value for the current row*/
    int size; /*!< The length of the row*/

public:
    /**
     * Constructor for creating a row with a certain length
     * @param length - desired length
     */
    explicit Row(int length);
    /**
     * Constructor for creating a row from a string
     * @param input - a string representing a row
     * @param maxColLength - the max word length for each column
     */
    explicit Row(const std::string& input, std::vector<int> &maxColLength, std::vector<Pair*> &formulas, int rowNum);
    /**
     * Returns the value of the cell at a certain position as plain text
     * @param pos - desired position
     * @return - value of the cell at pos
     */
    std::string get(int pos);
    /**
     * Returns the type of the cell at a certain position
     * @param pos - desired position
     * @return - type of cell at pos
     */
    Type getTypeOfCell(int pos);
    /**
     * Sets the value of a cell at a certain position
     * @param atRow - desired row
     * @param atCol - desired column
     * @param value - desired value
     * @param maxColLength - the max word length for each column
     * @param formulas - positions of all formula cells
     */
    void set(int atRow, int atCol,
            const std::string &value,
            std::vector<int> &maxColLength,
             std::vector<Pair *> &formulas);
    /**
     * Prints the row, formatted with a certain width
     * @param maxColLength - max length for each column in the table
     */
    void print(std::vector<int> &maxColLength);
    int getSize();
    std::string getFormula(int pos);
    /**
     * Converts the value of a Formula cell at a certain position
     * @param value - desired value
     * @param pos - position of the Formula cell
     */
    void convert(double value, int pos);
    /**
     * Fills a row with blank cells up to a certain length
     * @param len - desired length
     */
    void fillTillMax(int len);
    /**
     * Saves the content of the entire row into a file
     * @param file - desired file for ouptut
     */
    void save(std::ofstream &file);
    /**
     * Adds an empty cell at the end of a row
     */
    void addCell();
    /**
     * Destructor for manually deleting each cell in the current row
     */
    ~Row();
};


#endif //SPREADSHEET_ROW_H
