//
// Created by shrexpecial on 12.05.19.
//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>
#include "Formula.h"
#include "HelperFunctions.h"
//Constructor with parameters
Formula::Formula(std::string input) {
    //Get the substring without the "=" sign
    this->outputData = input;
    this->data = input.substr(1);
    this->type = FORMULA;
}
//Default constructor
Formula::Formula() {
    this->data = " ";
    this->type = FORMULA;
}
//Prints the value of the cell
void Formula::print() {
    if(std::isnan(this->value) || std::isinf(this->value)) std::cout << "ERROR";
    else std::cout << this->value;
}
//Sets the value of the cell
void Formula::set(std::string input) {
    this->data = input.substr(1);
}
//Returns the value of the cell as a string
std::string Formula::get() {
    if(std::isnan(this->value) || std::isinf(this->value)) return "ERROR";
    else return std::to_string(this->value);
}
//Returns the formula for the cell as a string
std::string Formula::getFormula() {
    return this->data;
}
//Sets the value of the formula
void Formula::convert(double val) {
    this->value = val;
}

void Formula::save(std::ofstream &file) {
    file << this->outputData;
}





