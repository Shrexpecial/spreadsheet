//
// Created by shrexpecial on 12.05.19.
//

#include <iostream>
#include "Double.h"
#include <iomanip>

Double::Double() {
    this->data = 0;
    this->type = DOUBLE;
}

Double::Double(double input) {
    this->data = input;
    this->type = DOUBLE;
}

void Double::print() {
    std::cout << this->data;
}

void Double::set(std::string a) {
    this->data = std::stod(a);
}

std::string Double::get() {
    return std::to_string(this->data);
}







