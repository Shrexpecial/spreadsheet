//
// Created by shrexpecial on 12.05.19.
//

#ifndef SPREADSHEET_HELPERFUNCTIONS_H
#define SPREADSHEET_HELPERFUNCTIONS_H

#include "Cell.h"
#include "Table.h"
#include <string>
#include <vector>
/**
 *
 * @param input - plain text value
 * @return - returns the cell type of the current input
 */
Type getTypeOfToken(std::string input);
/**
 * Trims a string from the right
 * @param s - string to trim
 */
void rtrim(std::string &s);
/**
 * Trims a string from the left
 * @param s - string to trim
 */
void ltrim(std::string &s);
/**
 * Splits a string into tokens based on a given delimiter
 * @tparam Out generic type parameter for the result
 * @param s - string to split
 * @param delim - desired delimiter
 * @param result
 */
template<typename Out>
void split(const std::string &s, char delim, Out result);
/**
 * Puts a split string into a vector
 * @param s - string to split
 * @param delim - desired delimiter
 * @return returns a vector filled with the tokens of the string
 */
std::vector<std::string> split(const std::string &s, char delim);
/**
 * Evaluates a formula and returns its value as plain text
 * @param table - the used table
 * @param formula - the formula as plain text
 * @return the value of the formula
 */
std::string evaluate(Table &table, std::string formula);

#endif //SPREADSHEET_HELPERFUNCTIONS_H
