//
// Created by shrexpecial on 12.05.19.
//
#include <iostream>
#include <iomanip>
#include <fstream>
#include "Cell.h"
#include "Table.h"
#include "HelperFunctions.h"
#include "Evaluator.h"

Table::Table() {
    this->height = 0;
    this->max_len = 0;
}

Table::Table(int height, int length) {
    for (int i = 0; i < height; ++i) {
        this->table.push_back(new Row(length));
        this->maxColLength.push_back(5);
    }
    this->max_len = length;
    this->height = height;
}

Table::Table(const std::string& filename) {
    std::ifstream file;
    file.open(filename, std::ios::app);
    if(file.is_open()){
        this->max_len = 0;
        this->height = 0;
        std::string item;
        while (getline(file, item)){
            this->table.push_back(new Row(item, this->maxColLength, this->formulas, this->height));
            this->height ++;
            if(this->table.at(height-1)->getSize() > this->max_len) max_len = this->table.at(height-1)->getSize();
        }
        std::cout << "Successfully opened " + filename << std::endl;
    }
    for (const auto &row : table) {
        row->fillTillMax(this->max_len);
    }
    file.close();
    convertFormulas();
}

void Table::print() {
    int pos = 0;
    int dashCounter = 5;
    std::cout << "R\\C |";
    for (int i = 0; i <this->max_len ; ++i) {
        std::cout << std::setw(this->maxColLength.at(i));
        dashCounter += this->maxColLength.at(i) + 1;
        std::cout << "C" + std::to_string(i+1) << "|";
    }
    std::cout << std::endl;
    for (int j = 0; j <dashCounter ; ++j) {
        std::cout << "-";
    }
    std::cout << std::endl;
    for (const auto &item : this->table) {
        std::cout << "R" <<std::setw(3) << std::setfill('0') << ++pos << + "|" <<std::setfill(' ');
        item->print(this->maxColLength);
        std::cout << std::endl;
    }
    std::cout << std::endl;
}
void Table::set(int h, int len, const std::string& value) {
    try {
        this->table.at(h - 1)->set(h - 1, len - 1, value, this->maxColLength, this->formulas);
        if(value.size() > this->maxColLength.at(len-1)) {
            this->maxColLength.at(len-1) = value.size();
        }
    }catch (std::exception &e){
        std::cout << "The cell you are trying to access is out of bounds." << std::endl;
    }
    convertFormulas();
}

std::string Table::get(int h, int l) {
    try{
        return this->table.at(h-1)->get(l-1);
    }catch (std::exception &e){
        return "0";
    }
}
void Table::save(std::ofstream &file) {
    for (const auto &item : table) {
        item->save(file);
    }
}
void Table::convertFormulas() {
    for (const auto &cell : this->formulas) {
        std::string form = evaluate(*this, this->table.at(cell->row)->getFormula(cell->col));
        Evaluator evaluator(form);
        double value = evaluator.getValue();
        this->table.at(cell->row)->convert(value,cell->col);
    }
}

Table::~Table() {
    for (const auto &item : table) {
        delete(item);
    }
    for (const auto &formula : formulas) {
        delete(formula);
    }
}

void Table::addRow() {
    this->table.push_back(new Row(max_len));
    this->height++;
    std::cout << "Added row R" << this->height << std::endl;
}

void Table::addColumn() {
    for (const auto &row : table) {
        row->addCell();
    }
    this->max_len++;
    std::cout << "Added column C" << this->max_len << std::endl;
    this->maxColLength.push_back(5);
}


