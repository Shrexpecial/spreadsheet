cmake_minimum_required(VERSION 3.14)
project(Spreadsheet)

set(CMAKE_CXX_STANDARD 14)

add_executable(Spreadsheet main.cpp Cell.cpp Cell.h Double.cpp Double.h String.cpp String.h Formula.cpp Formula.h  Integer.cpp Integer.h Row.cpp Row.h Table.cpp Table.h HelperFunctions.cpp HelperFunctions.h Spreadsheet.cpp Spreadsheet.h Evaluator.h Pair.h)