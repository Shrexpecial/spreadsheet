//
// Created by shrexpecial on 12.05.19.
//

#include <string>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <vector>
#include <iostream>
#include <cmath>
#include "HelperFunctions.h"
#include "Table.h"
using namespace std;

//Evaluate the cell type of the current token
Type getTypeOfToken(std::string input) {
    if(input[0] == '\"' && input[input.size()-1] == '\"')return STRING;
    else if(input[0] == '=') return FORMULA;
    else{
        try {
            std::string::size_type pt;
            int a = std::stoi(input, &pt);
            std::string rest = input.substr(pt);
            double b = std::stod(input, &pt);
            if(rest.empty()){
                return INTEGER;
            } else if(input.substr(pt).empty()){
                return DOUBLE;
            } else return ERROR;
        }
        catch (std::invalid_argument &arg){
            return BLANK;
        }

    }
}

//Trim white space from the right: Found on StackOverflow
void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

//Trim white space from the left: Found on StackOverflow
void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}


//Split the string into tokens using a specific delimiter: Found on StackOverflow
template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        rtrim(item);
        ltrim(item);
        *(result++) = item;
    }
}
//Split the string into tokens and put them in a vector: Found on StackOverflow
std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

std::string evaluate(Table &table, std::string formula) {
    std::string expression;
    for (int i = 0; i < formula.size(); ++i) {
        if(formula[i] == 'R'){
            std::string row, column;
            while(formula[++i] != 'C'){
                row += formula[i];
            }
            while (formula[++i] >= '0' && formula[i] <= '9' && i < formula.size()){
                column += formula[i];
            }
            int rowPos = std::stoi(row);
            int colPos = std::stoi(column);
            std::string current = table.get(rowPos, colPos);
            if(current.empty()){
                expression += "0 ";
            } else{
                std::string test;
                try{
                    test = table.get(rowPos, colPos);
                }catch(std::exception &e) {
                    expression += "0";
                }
                expression += test;
            }
            i--;
        } else expression += formula[i];
    }
    return expression;
}

