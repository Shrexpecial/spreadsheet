//
// Created by shrexpecial on 12.05.19.
//
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <fstream>
#include "Row.h"
#include "Integer.h"
#include "String.h"
#include "Formula.h"
#include "Double.h"
#include "Cell.h"
#include "HelperFunctions.h"

Row::Row(const std::string& input, std::vector<int> &maxColLength, std::vector<Pair*> &formulas, int rowNum) {
    std::vector<std::string> vec =  split(input, ',');
    this->size = 0;
    int pos = 0;
    for (const std::string& token : vec) {
        int wordLen = 0;
        switch (getTypeOfToken(token)) {
            case INTEGER:
                this->row.push_back(new Integer(std::stoi(token)));
                break;
            case DOUBLE:
                this->row.push_back(new Double(std::stod(token)));
                break;
            case STRING:
                this->row.push_back(new String(token));
                break;
            case FORMULA:
                formulas.push_back(new Pair{rowNum,pos});
                this->row.push_back(new Formula(token));
                break;
            case BLANK:
                this->row.push_back(new String());
                break;
            case ERROR:
                std::cout << token + " is not a valid type. Supported data types are:" << std::endl
                          << "STRING" << std::endl
                          << "INTEGER" << std::endl
                          << "DOUBLE" << std::endl;
                std::exit(1);
        }
        wordLen = this->row.at(pos)->getLen();
        try{
            if(maxColLength.at(pos) < wordLen) maxColLength.at(pos) = std::max(wordLen, 5);
        }catch (std::out_of_range &e){
            maxColLength.push_back(std::max(wordLen, 5));
        }
        pos++;
        this->size++;
    }
}

void Row::print(std::vector<int> &maxColLength) {
    int pos = 0;
    for (Cell *ptr : this->row) {
        std::cout << std::setw(maxColLength.at(pos++));
        ptr->print();
        std::cout << "|";
    }
}

std::string Row::get(int pos) {
    try {
        return this->row.at(pos)->get();
    }catch (std::exception &e){
        return "0";
    }
}

Row::Row(int length) {
    this->fillTillMax(length);
    this->size = length;
}

void
Row::set(int atRow, int atCol, const std::string &value, std::vector<int> &maxColLength, std::vector<Pair *> &formulas) {
    int wordLen;
    switch (getTypeOfToken(value)) {
        case INTEGER:
            this->row.at(atCol) = new Integer(std::stoi(value));
            break;
        case DOUBLE:
            this->row.at(atCol) = new Double(std::stod(value));
            break;
        case STRING:
            this->row.at(atCol)= new String(value);
            break;
        case FORMULA:
            formulas.push_back(new Pair{atRow, atCol});
            this->row.at(atCol) = new Formula(value);
            break;
        case BLANK:
            std::cout << "Error: " + value + " is an unknown data type. No changes have been made." << std::endl ;
            break;
        case ERROR:
            std::cout << "Error: " + value + " is an unknown data type. No changes have been made." << std::endl ;
            break;
    }
    try{
        wordLen = this->row.at(atCol)->getLen();
        if(maxColLength.at(atCol) <= wordLen) maxColLength.at(atCol) = wordLen;
    }catch (std::out_of_range &e){
    }

}

int Row::getSize() {
    return this->size;
}

void Row::fillTillMax(int len) {
    while (this->row.size() < len) this->row.push_back(new String());
}

Row::~Row() {
    for (const auto &item : this->row) {
        delete(item);
    }
}

Type Row::getTypeOfCell(int pos) {
    return this->row.at(pos)->getType();
}

void Row::convert(double value, int pos) {
    this->row[pos]->convert(value);
}

void Row::save(std::ofstream &file) {
    for (const auto &item : row) {
        item->save(file);
        file << ",";
    }
    file << std::endl;
}

std::string Row::getFormula(int pos) {
    return this->row.at(pos)->getFormula();
}

void Row::addCell() {
    this->row.push_back(new String());
}



