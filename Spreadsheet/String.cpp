//
// Created by shrexpecial on 12.05.19.
//

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <fstream>
#include "String.h"


String::String(std::string input) {
    this->outputData = input;
    input = input.substr(1,input.size()-2);
    std::string final;
    for (int i = 0; i < input.size(); ++i) {
        if(input[i] == '\\'){
            final += input[++i];
        } else final += input[i];
    }
    this->data = final;
    this->type = STRING;
}

String::String() {
    this->data = "";
    this->outputData = "";
    this->type = STRING;
}

void String::print() {
    std::cout << this->data;
}

void String::set(std::string input) {
    this->data = input;
}

std::string String::get() {
    return this->data;
}

void String::save(std::ofstream &file) {
    file << this->outputData;
}

